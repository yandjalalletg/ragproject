import React from 'react';
import { useSelector } from 'react-redux';
import Image from 'react-bootstrap/Image';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
const SingleInnovation = ({ id }) => {
  const { data } = useSelector((state) => state.innovation);
  const singleInnovation = data.data.filter((item) => item.id == id);
  const innovationData = singleInnovation[0];

  return (
    <Container>
      <Innovation>
        <Image src={innovationData.image} width="50%" rounded />
        <Description dangerouslySetInnerHTML={{ __html: innovationData.description }} />
      </Innovation>
    </Container>
  );
};
const Innovation = styled.div`
  padding-left: 50px;
  padding-top: 20px;
`;
const Description = styled.p`
  text-align: justify;
  font-family: 'Times New Roman';
  padding-top: 80px;
`;
export default SingleInnovation;
