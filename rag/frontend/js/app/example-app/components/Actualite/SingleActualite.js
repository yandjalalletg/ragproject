import React from 'react';
import { useSelector } from 'react-redux';
import Image from 'react-bootstrap/Image';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
const SingleAcceuil = ({ id }) => {
  const { data } = useSelector((state) => state.actualite);
  const singleActualite = data.data.filter((item) => item.id == id);
  const accueilData = singleActualite[0];

  return (
    <Container>
      <Acceuil>
        <Image src={accueilData.image} width="50%" rounded />
        <Description dangerouslySetInnerHTML={{ __html: accueilData.description }} />
      </Acceuil>
    </Container>
  );
};
const Acceuil = styled.div`
  padding-left: 50px;
  padding-top: 20px;
`;

const Description = styled.p`
  text-align: justify;
  font-family: 'Times New Roman';
  padding-top: 80px;
`;
export default SingleAcceuil;
