import React from 'react';
import { useSelector } from 'react-redux';
import Image from 'react-bootstrap/Image';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
const SinglePolitique = ({ id }) => {
  const { data } = useSelector((state) => state.politique);
  const singlePolitique = data.data.filter((item) => item.id == id);
  const politiqueData = singlePolitique[0];

  return (
    <Container>
      <Politique>
        <Image src={politiqueData.image} width="50%" rounded />
        <Description dangerouslySetInnerHTML={{ __html: politiqueData.description }} />
      </Politique>
    </Container>
  );
};
const Politique = styled.div`
  padding-left: 50px;
  padding-top: 20px;
`;
const Description = styled.p`
  text-align: justify;
  font-family: 'Times New Roman';
  padding-top: 80px;
`;
export default SinglePolitique;
