import * as React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import Fade from 'react-reveal';

import ParticlesBg from 'particles-bg';
//import ActualiteItem from './actualityItem';
//import { fetchAcceuilData } from './slice';
import background from './logo_acc.jpg';
const Accueil = () => {
  const dispatch = useDispatch();
  const { data } = useSelector((state) => state.acceuil);

  // React.useEffect(() => {
  //   dispatch(fetchAcceuilData())
  //     .unwrap()
  //     .then((response) => {
  //       if (fetchAcceuilData.fulfilled.match(response))
  //         dispatch({
  //           type: 'acceuil/fetchAcceuilInfoRequested/fulfilled',
  //           payload: response.data,
  //         });
  //     })
  //     .catch((error) => {
  //       dispatch({
  //         type: 'acceuil/fetchAcceuilInfoRequested/rejected',
  //         payload: error.message,
  //       });
  //     });
  // }, []);

  return (
    <Div>
      <ParticlesBg type="circle" bg={true} />
      <Actualites>
        <div>
          Restoring Africa’s Greatness, est un magazine digital créé pour Vous et les Générations
          futures. C'est le moment opportun pour sensibiliser les afro descendants concernés et
          engagés qui croient que nous devons apporter des changements importants aux enjeux
          continentaux fondamentaux auxquels notre continent est confronté aujourd'hui. Avoir une
          attitude positive et être informé et bien informé sur les questions clés sera bénéfique
          pour vous et les autres et vous aidera à vous efforcer d'être le meilleur possible !
          <br />
          <br />
          Notre magazine à pour objectif principal de vous :{' '}
          <Fade left>
            {' '}
            <h1 className="responsive-headline">
              Inspirer, informer et célébrer ceux qui réinventent l’Afrique : les entrepreneurs et
              les leaders africains.
            </h1>
          </Fade>
          <br />
          <br />
          Vous trouverez dans ce magazine :
          <br />
          <ul>
            <li>
              Des informations concernant des innovations technologiques, sociales et économiques
              sur le continent africain
            </li>
            <li>
              {' '}
              Conseils d'apprentissage de la vie pour aider à responsabiliser, inspirer et motiver.
            </li>
            <li>
              Questions fondamentales continentales mettant en évidence certains points de
              discussion sur les problèmes auxquels notre continent est confronté aujourd'hui.
            </li>
            <li>
              {' '}
              Des informations pour un accès facile pour être plus au courant des grands changement
              positifs qui s’opèrent sur le continent africain
            </li>
          </ul>
          En tant qu’africains, il est de notre responsabilité d'être informés et d'avoir des
          discussions pour continuer à redonner l’Afrique sa grandeur. Nous pouvons créer un point
          de basculement pour aller de l'avant dans notre approche positive de l'Afrique. <br />
          <br />
          <b>Dabekoa YARBONME</b>
        </div>
        {/* {data.data &&
          data.data.map((actualite) => <ActualiteItem key={actualite.id} actualite={actualite} />)} */}
      </Actualites>
    </Div>
  );
};
const Div = styled.div`
  background-repeat: no-repeat;
  background-position: center;
  padding: 20px;
`;
const Actualites = styled.div`
  display: flex;
  /* justify-content: center; */
  flex-wrap: wrap;
  /* width: 100%; */
  margin-left: 50px;
  margin-right:20px
  text-align: justify;
  font-family: 'Times New Roman';
`;

export default Accueil;
