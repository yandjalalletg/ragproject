import React from 'react';
import { useSelector } from 'react-redux';
import Image from 'react-bootstrap/Image';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
const SingleInterview = ({ id }) => {
  const { data } = useSelector((state) => state.interview);
  const singleInterview = data.data.filter((item) => item.id == id);
  const interviewData = singleInterview[0];

  return (
    <Container>
      <Interview>
        <Image src={interviewData.image} width="50%" rounded />
        <Description dangerouslySetInnerHTML={{ __html: interviewData.description }} />
      </Interview>
    </Container>
  );
};
const Interview = styled.div`
  padding-left: 50px;
  padding-top: 20px;
`;
const Description = styled.p`
  text-align: justify;
  font-family: 'Times New Roman';
  padding-top: 80px;
`;
export default SingleInterview;
