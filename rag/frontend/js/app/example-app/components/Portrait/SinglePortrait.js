import React from 'react';
import Image from 'react-bootstrap/Image';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
const SinglePortrait = ({ id }) => {
  const { data } = useSelector((state) => state.portrait);
  const singlePortrait = data.data.filter((item) => item.id == id);
  const portraitData = singlePortrait[0];
  return (
    <Container>
      <Portrait>
        <Image src={portraitData.image} width="50%" rounded />
        <Description dangerouslySetInnerHTML={{ __html: portraitData.description }} />
      </Portrait>
    </Container>
  );
};
const Portrait = styled.div`
  padding-left: 50px;
  padding-top: 20px;
`;
const Description = styled.p`
  text-align: justify;
  font-family: 'Times New Roman';
  padding-top: 80px;
`;
export default SinglePortrait;
