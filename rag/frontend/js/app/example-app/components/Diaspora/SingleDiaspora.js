import React from 'react';
import { useSelector } from 'react-redux';
import Image from 'react-bootstrap/Image';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
const SingleDiaspora = ({ id }) => {
  const { data } = useSelector((state) => state.diaspora);
  const singleDiaspora = data.data.filter((item) => item.id == id);
  const diasporaData = singleDiaspora[0];

  return (
    <Container>
      <Diaspora>
        <Image src={diasporaData.image} width="50%" rounded />
        <Description dangerouslySetInnerHTML={{ __html: diasporaData.description }} />
      </Diaspora>
    </Container>
  );
};
const Diaspora = styled.div`
  padding-left: 50px;
  padding-top: 20px;
`;
const Description = styled.p`
  text-align: justify;
  font-family: 'Times New Roman';
  padding-top: 80px;
`;
export default SingleDiaspora;
