import React from 'react';
import { useSelector } from 'react-redux';
import Image from 'react-bootstrap/Image';
import styled from 'styled-components';
import { Container } from 'react-bootstrap';
const SingleStartup = ({ id }) => {
  const { data } = useSelector((state) => state.startup);
  const singleStartup = data.data.filter((item) => item.id == id);
  const startupData = singleStartup[0];

  return (
    <Container>
      <Startup>
        <Image src={startupData.image} width="50%" rounded />
        <Description dangerouslySetInnerHTML={{ __html: startupData.description }} />
      </Startup>
    </Container>
  );
};
const Startup = styled.div`
  padding-left: 50px;
  padding-top: 20px;
`;
const Description = styled.p`
  text-align: justify;
  font-family: 'Times New Roman';
  padding-top: 80px;
`;
export default SingleStartup;
